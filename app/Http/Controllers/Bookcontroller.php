<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Http\Requests;

use LucaDegasperi\OAuth2Server\Authorizer;

class Bookcontroller extends Controller
{
    public function index()
    {
        return Book::all();
    }

	// public function index(Authorizer $authorizer)
	// {

	//     $user_id=$authorizer->getResourceOwnerId(); // the token user_id
	//     $user=\App\User::find($user_id);// get the user data from database
	//     $posts= \App\Post::where('user_id','=',$user->id);
	// }

    public function show($id)
    {
        return Book::findOrFail($id);
    }


}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('oauth/access_token', function() {
 return Response::json(Authorizer::issueAccessToken());
});

Route::get('/register',function(){$user = new App\User();
 $user->name="hasrul";
 $user->email="hs@gmail.com";
 $user->password = \Illuminate\Support\Facades\Hash::make("hasrul");
 $user->save();
});

Route::group(['middleware' => 'oauth'], function()
{
	Route::get('token', function() {
		return "tes";
	});
    Route::get('/book',  'BookController@index');
 //    $user_id=Authorizer::getResourceOwnerId(); // the token user_id
	// $user=\App\User::find($user_id);// get the user data from database
	// return Response::json($user);
});